package virta.examples.helloservlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.webjet.servlet.WebJetServlet;

/**
 * Hello Servlet for example and test
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class HelloServlet extends WebJetServlet {

	private static final long serialVersionUID = -2199194612023866771L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		//Note: you can skip it, if you prefer use own logging, instead of server-side logging 
		super.sendRequest(req);
		
		//Note: you can use all Servlets API (ver. 3.0) methods as usual 
		resp.setHeader("Served-by","HelloServlet");
		
		//Note: this call is compulsory, otherwise ServletException will thrown
		//Modifying resp after setResponce, not good idea after all 
		super.sendResponse(resp);
		
		//Use reply method to send data 
		super.reply("<h1>HelloServlet works!</h1>");
	}
}
