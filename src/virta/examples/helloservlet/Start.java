package virta.examples.helloservlet;

import virta.webjet.server.Server;

public class Start {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//init
		Server server = new Server(args);
		
		
		//make config
		server.setHandler("/","virta.examples.helloservlet");
		
		//adding servlets
		server.addServlet("HelloServlet","/");
		
		//call start
		server.start();
	}

}
